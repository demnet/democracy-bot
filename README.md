# DemNet's Democracy Bot
The goal of this project is to create a bot
infrastructure, that can interact
with several chat applications to govern
these in a democratic manner.

Initially this project will focus
on Discord and Matrix, but may extend to
be used with Signal, Telegram, Slack and other
such applications later.

### Self-governance
After adding a bot to a channel, server
or room, this bot gains special administrative
privileges.

These privileges are only used
upon the vote of the users of the
channel.

### Absolute majority single motion voting
The voting process begins with a proposal
being made in a community.

Then each users can vote once
in favor or against the proposal.

The proposal is accepted, if
an absolute majority of users votes
in favor and it is rejected if an
absolute majority of users votes against it.

Thus there is no time limit on any proposal,
since a proposal will be implemented as soon
as it's received an absolute majority.

### What can be voted on?
Votes can be held on:
- Appointments (awarding Roles)
- Documents as Resolutions
- Banning and inviting members.

It'll be started to implement and test this by implementing
Resolutions passing, then Appointments and then banning and invitations.


### Architecture
The architecture has three parts:
1. The Core
2. The Interfaces
3. The `main.js` file

While the Core defines a simple and
single documented API to the democracy bots,
the interfaces map this API to the API of
the messenger (like Discord or Matrix).

The `src/main.js` then connects the interfaces
with the core and runs the actual server.

### The Database
As a database, this bot uses OrbitDB,
but without connecting to the network.

But it should be encrypted at rest anyway.


# Example communications using this Bot:
### Appointments
```
> /appoint @csdummi moderator
Proposal #1: Appoint @csdummi to moderator role
In favor: 0   In opposition: 0  Voters: 5
> /vote for #1
...
Timed out on proposal to appoint @csdummi to
moderator role.
Results:
In favor: 1 In opposition: 0  Voters: 5
Proposal failed.
```
Commands: `appoint`, `dismiss`

### Publish
```
> /publish https://example.org/link/to/file/to/publish
Proposal #2:
Publish a File at https://example.org/link/to/file/to/publish
as group.
In favor: 0 In opposition: 0  Voters: 5  
> /vote for #2
...
Timed out on proposal to publish
File at https://example.org/link/to/file/to/publish
as a group.
Results:
In favor: 3 In opposition: 2  Voters: 5
Proposal succeed.
You can now find the file at:
https://democraticnet.de/<proto>/<id>/<CID of url>
```

Commands: `publish`, `unpublish`

### Invite
```
> /invite @wizabra
Proposal #3:
Invite @wizabra to the group.
In favor: 0 In opposition: 0  Voters: 5
> /vote for inviting @wizabra
...
Timed out on proposal to
invite @wizabra.
Results:
In favor: 4 In opposition: 0  Voters: 5
Invited @wizabra
```

Commands: `invite`, `ban`


## The Commands
There are 3*2 = 6 commands,
two being the inverse of each other.

2 for appointing users to roles.

2 for publishing files as a group.

2 for inviting and banning users.

Using these commands you can
indirectly administer the entire
group. But not directly.

So, for example, we might have the
ability to appoint people to
roles, but no way of creating those roles
and giving them permissions.

In order to make this possible, the
bot always creates two different roles:

1. An Administrator role with the permission
to create other roles and give them  permissions.
2. A Moderator role with the permission
to delete messages by other users.

You could compare the Administrator to
the president of the Group and a Moderator
as it's supreme court.
There can be multiple Admins and Mods
in a group.

Thus a conversation with this bot can
be started by appointing two (different?)
users to the Admin and moderator role.

```
> /appoint @csdummi administrator
Proposal #0:
Appoint @csdummi to administrator role.
In favor: 0 In opposition: 0  Voters: 5  
> /appoint @wizabra moderator
Proposal #1:
Appoint @wizabra to moderator role.
In favor: 0 In opposition: 0  Voters: 5
```
And if both proposals pass,
new roles can be created.

### Voting command
You vote with the `/vote`
command, followed by either `for` or `against`
and then followed by the propsoal id (what
was written after Proposal when making the proposal.).

If the protocol permits, this message should not
be shown to anybody else and be stored
anonymously in the database to ensure
the best possible secrecy of the ballots.

### Removing democracy
If groups want to remove democracy
from their system, they can make a proposal:

```
> /abolish democracy
Proposal #1001:
Abolish Democracy, remove
this bot from the group
and grant full administrative
privileges to the administrators.
In favor: 0 In opposition: 0  Voters: 5
> /vote for #1001
...
Timed out on proposal to
abolish democracy, remove
this bot from the group
and grant full administrative
privileges to the administrators.
Results:
In favor: 1 In opposition: 4  Voters: 5
Proposal failed.
```
